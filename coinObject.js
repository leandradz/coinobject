const main = document.createElement("main");
const div = document.createElement("div");
const divImg = document.createElement("div");
const divMsg = document.createElement("div");
const h1 = document.createElement("h1");
const btn = document.createElement("input");
btn.setAttribute("type", "button");
h1.innerText = 'BitCoin'
divMsg.innerText = 'Jogar Moeda'
divMsg.id = 'playCoin'
main.appendChild(h1)
main.appendChild(div);
main.appendChild(divImg);
document.body.appendChild(main);
document.body.appendChild(btn);
document.body.appendChild(divMsg)

btn.addEventListener("click", function(event){
    location.reload()
})

const coin = {
  state: 0,
  flip: function () {
    this.state = Math.floor(Math.random() * 2);
  },
  toString: function () {
    if (this.state === 0) {
      return "Heads";
    }
    return "Tails";
  },
  toHTML: function () {
    const image = document.createElement("img");
    divImg.appendChild(image);
    if (this.state === 0) {
      return (image.id = "heads");
    }
    return (image.id = "tails");
  },
};

function display20Flips() {
  const results = [];
  for (let i = 0; i < 20; i++) {
    coin.flip();
    div.innerText += ` ${coin.toString()} `;
    results.push(coin.toString());
  }
  return results;
}

function display20Images() {
  const results = [];
  for (let i = 0; i < 20; i++) {
    coin.flip();
    coin.toHTML();
    results.push(coin.toString());
  }
  return results;
}

display20Flips();
display20Images();
